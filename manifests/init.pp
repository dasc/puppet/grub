class grub (
    Integer $timeout,
    String $distributor,
    String $default_entry,
    Boolean $disable_submenu,
    String $terminal_output,
    String $cmdline_linux,
    Boolean $disable_recovery,
) {
    file { '/etc/default/grub':
	ensure => 'file',
	owner => 'root',
	group => 'root',
	mode => '0644',
	content => epp('grub/defaults.epp', {
	    'timeout' => $timeout,
	    'default_entry' => $default_entry,
	    'disable_submenu' => $disable_submenu,
	    'terminal_output' => $terminal_output,
	    'cmdline_linux' => $cmdline_linux,
	    'disable_recovery' => $disable_recovery,
	}),
    }

    exec {'grub2-mkconfig' :
        command => '/sbin/grub2-mkconfig -o /boot/grub2/grub.cfg',
	refreshonly => true,
	subscribe => File['/etc/default/grub'],
    }
}
